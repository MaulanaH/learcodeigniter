<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    private $_table = "user";

    public $username;
    public $password;
    public $status;

    public function rules()
    {
        return [
            ['field' => 'username',
            'label' => 'username',
            'rules' => 'required'],

            ['field' => 'password',
            'label' => 'password',
            'rules' => 'required'],
        ];
    }

    public function save()
    {
        $post = $this->input->post();
        $data['username'] = $this->input->post('username');
        $data['password'] = $this->input->post('password');
        $data['status'] = $this->input->post('status');
        
        $this->db->insert($this->_table, $data);
    }
}
