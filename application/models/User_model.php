<?php

class User_model extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->load->database();
    }

    private $_table = "user";
    
    public $id;
    public $username;
    public $password;
    public $status;

    public function cek_user($data) {
        $query = $this->db->get_where('user', $data);
        return $query;
    }

    public function rules()
    {
        return [
            ['field' => 'username',
            'label' => 'username',
            'rules' => 'required'],

            ['field' => 'password',
            'label' => 'password',
            'rules' => 'required'],
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->status = $post["status"];
        $this->username = $post["username"];
        $this->password = $post["password"];
        $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }
}

?>