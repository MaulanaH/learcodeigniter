<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item <?php echo $this->uri->segment(2) == '' ? 'active': '' ?>">
        <a class="nav-link" href="<?php echo site_url('admin') ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Home</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/Supplier') ?>">
            <i class="fas fa-fw fa-user"></i>
            <span>Customer</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/Products') ?>">
            <i class="fas fa-fw fa-table"></i>
            <span>Product</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/User') ?>">
            <i class="fas fa-fw fa-user"></i>
            <span>User</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('admin/Register') ?>">
            <i class="fas fa-fw fa-user"></i>
            <span>Register</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-cog"></i>
            <span>Settings</span></a>
    </li>

    
</ul>
