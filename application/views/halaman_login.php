<!-- <form action='<?php echo base_url('index.php/login/proses_login') ?>' method='post'> 
      <div class='form-group has-feedback'> 
        <input type='text' class='form-control' placeholder='Username atau Email' name='username' required id='username'> 
        <span class='glyphicon glyphicon-user form-control-feedback'></span> 
      </div> 
      <div class='form-group has-feedback'> 
        <input type='password' class='form-control' placeholder='Password' name='password' required id='password'> 
        <span class='glyphicon glyphicon-lock form-control-feedback'></span> 
      </div> 
      <div class='row'> 
        <div class='col-xs-8'> 
          <div class='checkbox icheck'> 
            <label> 
              <input type='checkbox'> Ingat Saya 
            </label> 
          </div> 
        </div> 
        <!– /.col –> 
        <div class='col-xs-4'> 
          <button type='submit' class='btn btn-primary btn-block btn-flat'>Masuk</button> 
        </div> 
        <!– /.col –> 
      </div> 
    </form> -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Admin</title>

    <!-- Bootstrap core CSS-->
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 text-center mt-5 mx-auto p-4">
                <h1 class="h2">Login Admin Product</h1>
                <p class="lead">Silahkan masuk</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-5 mx-auto mt-5">
                <form action='<?php echo base_url('index.php/login/proses_login') ?>' method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name='username' required id='username' placeholder="Silahkan input..." required />
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name='password' required id='password' placeholder="Password.." required />
                    </div>
                    <div class="form-group">
                        <div class="d-flex justify-content-between">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" name="rememberme" id="rememberme" />
                                <label class="custom-control-label" for="rememberme"> Ingat Saya</label>
                            </div>
                            <a href="<?= site_url('reset_password') ?>">Lupa Password?</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success w-100" value="Login" />
                    </div>

                </form>
            </div>
        </div>
    </div>

</body>

</html>