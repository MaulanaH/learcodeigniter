<?php
Class Login extends CI_Controller{

    public function index(){
        $this->load->view('halaman_login');
    }

    public function proses_login() {
    $data = array('username' => $this->input->post('username', TRUE),
                  'password' => $this->input->post('password', TRUE),
                );
    $this->load->model('User_model');
    $hasil = $this->User_model->cek_user($data);
    $this->session->set_userdata($data);
    if ($hasil->num_rows() == 1) {
        redirect('admin');
    } else {
        echo "<script>alert('Gagal login: Cek Username atau Password');history.go(-1)</script>";
        // redirect('login');
    }
  }

  public function logout(){
    $this->session->sess_destroy(); // Hapus semua session
    redirect('login'); // Redirect ke halaman login
  }

}

?>
