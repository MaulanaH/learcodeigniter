<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("register_model");
        $this->load->library('form_validation');
        if( ! $this->session->userdata('username')) // Jika tidak ada
            redirect('login'); // Redirect ke halaman login
    }

    public function index()
    {
        $this->load->view("admin/new_form");
    }

    public function add()
    {
        $register = $this->register_model;
        $validation = $this->form_validation;
        $validation->set_rules($register->rules());

        if ($validation->run()) {
            $register->save();
            $this->session->set_flashdata('success', 'Berhasil daftar');
        }

        $this->load->view("admin/new_form");
    }
}