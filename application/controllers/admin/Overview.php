<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		if( ! $this->session->userdata('username')) // Jika tidak ada
            redirect('login'); // Redirect ke halaman login
    }
	

	public function index()
	{
        // load view admin/overview.php
        $this->load->view("admin/overview");
	}
}
